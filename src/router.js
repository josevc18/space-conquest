import { createWebHistory, createRouter } from "vue-router";
import MenuScreen from './components/screens/MenuScreen.vue'
import GameScreen from './components/screens/GameScreen.vue'
import StoreScreen from './components/screens/StoreScreen.vue'
const routes = [
    { path: '/', component: MenuScreen },
    { path: '/game', component: GameScreen },
    { path: '/store', component: StoreScreen },
]
const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router