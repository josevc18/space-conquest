// gamica
import { utilities, gamica } from '../../../gamica-engine/src/index'

let Vec2 = utilities.Vec2
let renderer = new gamica.Renderer(60)
let eManager = new gamica.EntityManager()


let layers = {
    probesLayer: new gamica.Layer({ zIndex: 5, world: { width: 8000, height: 8000 } }).toWindowSize(),
    probesTrailLayer: new gamica.Layer({ zIndex: 0, world: { width: 8000, height: 8000 } }).toWindowSize(),
    planetLayer: new gamica.Layer({ zIndex: 3, world: { width: 8000, height: 8000 } }).toWindowSize(),
    starFieldLayer: new gamica.Layer({ zIndex: 1, world: { width: 8000, height: 8000 } }).toWindowSize(),
}




export { renderer, eManager, Vec2, utilities, gamica, layers }