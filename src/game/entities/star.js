import { gamica, layers, Vec2 } from "../../game";
import Star from "./probe";
export default class Planet extends gamica.Entity {
    constructor() {
        super();
        this.position = new Vec2(0, 0).randomize(-2000, 2000)
        this.toWorldPosition = layers.planetLayer.toWorldSpace(this.position)
        this.radius = Math.random() * (0.5 - 0.1) + 1
        this.layer = layers.starFieldLayer
        this.surfaceColor = 'white'

    }
    draw() {
        this.layer.ctx.save()
        this.layer.ctx.beginPath();
        this.layer.ctx.shadowColor = 'red';
        this.layer.ctx.shadowBlur = 15;
        this.layer.ctx.fillStyle = 'rgba(255,255,255,.5)';
        this.layer.ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2)
        this.layer.ctx.fill()

        this.layer.ctx.restore();


    }

    start() {
        this.addTag('start')
    }
    update(time) {
        this.draw()

    }
}