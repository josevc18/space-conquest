import { gamica, layers, Vec2 } from "../../game";
import Trail from "./trail";
export default class Probe extends gamica.Entity {
    constructor({ position = new Vec2(0, 0), alliance = "player", objective = undefined } = {}) {
        super();
        this.position = position
        this.toWorldPosition = layers.planetLayer.toWorldSpace(this.position)
        this.size = 10;
        this.angle = 0
        this.layer = layers.probesLayer
        this.surfaceColor = '#F45634'
        this.speed = 0.5
        this.objective = objective
        this.mouse = new Vec2(0, 0)
        this.sprite = new gamica.SpriteSheet({ path: '/src/res/ships/ship_1.png' })
    }
    draw() {

        this.layer.ctx.save()
        this.layer.rotate(this.position.x, this.position.y, this.angle * Math.PI / 180)
        // this.layer.ctx.fillStyle = this.surfaceColor;
        this.layer.ctx.drawImage(this.sprite.spriteElem, this.position.x, this.position.y, this.size, this.size)
        // this.layer.ctx.fill()
        this.layer.ctx.restore();
    }
    start() {
        this.alliance === 'player' ? this.addTag('player_probe') : this.addTag('enemy_probe')
        window.addEventListener('mousemove', (e) => {
            this.mouse.x = e.clientX
            this.mouse.y = e.clientY
            this.mouseMove = true
        })

    }
    update(time) {
        // this.entityManager.addEntity(new Trail({
        //     position: new Vec2(
        //         this.position.x + 5,
        //         this.position.y + 5,
        //     )
        // }))
        this.draw()
        this.angle = this.position.pointToDegs(this.objective.position)
        this.position.movePointingToDegs(this.angle, 0.05 * this.rendererTime.deltaTime, 0.05 * this.rendererTime.deltaTime)
        // see if collided with objective
        if (this.position.distance(this.objective.position) < 80) {

            if (this.objective.tags.has('enemy_planet')) {
                this.objective.units--

                if (this.objective.units < 1) {
                    this.objective.removeTag('enemy_planet')
                    this.objective.addTag('player_planet')
                    this.objective.restockUnits.play()
                }
            } else {
                this.objective.units++
            }

            this.destroy()

        }

    }
}