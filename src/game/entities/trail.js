import { gamica, layers, Vec2, utilities } from "../../game";

export default class Trail extends gamica.Entity {
    constructor({ position = new Vec2(0, 0) } = {}) {
        super();
        this.position = position
        this.layer = layers.probesLayer
        this.lifeTime = 1000
        this.opacity = 1

    }
    draw() {
        layers.probesTrailLayer.ctx.save()
        layers.probesTrailLayer.ctx.fillStyle = `rgba(240,241,171,${this.opacity})`;
        layers.probesTrailLayer.ctx.fillRect(this.position.x, this.position.y, 2, 2)
        layers.probesTrailLayer.ctx.fill()
        layers.probesTrailLayer.ctx.restore();

    }
    start() {
        let test = { opacity: 100 }
        this.trailAnimation = utilities.anime({
            targets: test,
            opacity: 0,
            easing: 'linear',
            round: true,
            loop: false,
            autoplay: true,
            duration: 1000,
            update: (a) => {
                
                if (a.animations[0].currentValue < 5) {
                    this.trailAnimation.pause()
                    this.destroy()
                }
                this.opacity = `0.${a.animations[0].currentValue}`

            },
        });
    }
    update(time) {
        this.draw()

    }
}