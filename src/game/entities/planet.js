import { EMPTY_ARR, toTypeString } from "@vue/shared";
import { gamica, layers, Vec2, utilities } from "../../game";
import Probe from "./probe";
export default class Planet extends gamica.Entity {
    constructor({ faction = "player" } = {}) {
        super();
        this.position = new Vec2(0, 0).randomize(0, 2000)
        this.toWorldPosition = layers.planetLayer.toWorldSpace(this.position)
        this.radius = Math.floor(Math.random() * (50 - 15) + 16)
        this.layer = layers.planetLayer
        this.surfaceColor = Math.random() * (360 - 0)
        this.units = 25;
        this.maxUnits = 25;
        this.selected = false
        this.mouse = new Vec2(0, 0)
        this.mouseClick = false
        this.mouseMove = false
        this.attacking = false
        this.faction = faction

    }

    draw() {
        this.layer.ctx.save()
        this.layer.ctx.beginPath();
        this.layer.ctx.fillStyle = `hsl(${this.surfaceColor}, 90%,65%)`;
        this.layer.ctx.strokeStyle = `hsl(${this.surfaceColor}, 90%,45%)`;
        this.layer.ctx.lineWidth = 8


        this.layer.ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2)
        this.layer.ctx.fill()
        this.layer.ctx.stroke()

        this.layer.ctx.restore();

    }


    // check naivly the overlapping of any other planet in the game
    checkOverlapping() {
        let overlapping = this.entityManager.queryEntities(ent => {
            return ent.tags.has('planet')
        }).map(planet => {
            return planet.position.distance(this.position) < this.radius && planet !== this
        })
        console.log(overlapping.includes(true))
        return overlapping.includes(true)
    }
    attackPlanet() {
        if (this.attacking === false) {
        }
    }
    start() {
        this.addTag('planet')
        this.faction === "player" ? this.addTag('player_planet') : this.addTag(this.faction).addTag('enemy_planet')
        console.log(this.faction)
        // snap to grid
        this.position.x -= this.position.x % 200
        this.position.y -= this.position.y % 200

        window.addEventListener('mousemove', (e) => {
            this.mouse.x = e.clientX
            this.mouse.y = e.clientY
            this.mouseMove = true
        })

        window.addEventListener('mouseup', (e) => {


        })

        window.addEventListener('mousedown', (e) => {
            let selectedPlanets = this.entityManager.queryEntities((entity) => {
                if (entity.tags.has('mouse_selected')) return entity
            })

            if (this.toWorldPosition.distance(this.mouse) < this.radius) {

                if (this.selected === false) {
                    this.selected = true
                    this.addTag('mouse_selected')
                }
                selectedPlanets = this.entityManager.queryEntities((entity) => {
                    if (entity.tags.has('mouse_selected')) return entity
                })
                if (selectedPlanets.length === 1) {
                    this.addTag('selected_first')

                }
                if (selectedPlanets.length === 2) {
                    this.addTag('selected_second')
                    // attack logic

                    let objectives = {
                        first: undefined,
                        second: undefined
                    }
                    selectedPlanets.map(ent => {
                        if (ent.tags.has('selected_first')) { objectives.first = ent }
                        if (ent.tags.has('selected_second')) { objectives.second = ent }
                    })
                    // attack planet
                    if (objectives.first.units > 0 && objectives.first.tags.has('player_planet')) {
                        objectives.first.objective = objectives.second
                        objectives.first.drainUnits.play()
                    }
                }

            } else if (this.toWorldPosition.distance(this.mouse) > this.radius) {
                // test if click was in other planer
                let isInPlanet = selectedPlanets.map(ent => {
                    return ent.position.distance(this.mouse) < ent.radius
                })
                if (!isInPlanet.includes(true) && isInPlanet.length === 2
                ) {
                    selectedPlanets.map(ent => {
                        ent.selected = false;
                        ent.removeTag('selected_first')
                        ent.removeTag('selected_second')
                        ent.removeTag('mouse_selected')
                    })
                }

            }





        })

        // animations
        this.drainUnits = utilities.anime({
            targets: { units: 1 },
            units: 0,
            easing: 'linear',
            round: 1,
            loop: this.units,
            duration: 100,
            loopBegin: () => {
                new Vec2(this.position.x,
                    this.position.y)
                    .randomize(this.toWorldPosition.y, this.toWorldPosition.x)

                this.entityManager.addEntity(new Probe({
                    position: new Vec2(
                        Math.random() * ((this.position.x - this.radius) -
                            (this.position.x + this.radius)
                        ) + (this.position.x + this.radius),

                        Math.random() * ((this.position.y - this.radius) -
                            (this.position.y + this.radius)
                        ) + (this.position.y + this.radius),

                    )

                    , objective: this.objective
                }))
                this.units--

                if (this.units === 0) {
                    this.restockUnits.play()
                    this.drainUnits.pause()

                }



            },
            autoplay: false
        });


        // animations
        this.restockUnits = utilities.anime({
            targets: { units: 1 },
            units: 1,
            easing: 'linear',
            round: 1,
            delay: 100,
            loop: this.maxUnits,
            autoplay: false,
            duration: 900,
            loopBegin: () => {
                this.units++
            },
        });




    }
    mouseFuncs() {



    }
    update(time) {
        this.mouseMove = false
        this.toWorldPosition = layers.planetLayer.toWorldSpace(this.position)
        this.mouseFuncs()
        this.draw()
        // drain units

    }
}