import Vec2 from "./vec2";
import hslToRgb from "./hslToRgb";
import anime from 'animejs/lib/anime.es.js';
export const utilities = { hslToRgb, Vec2, anime }