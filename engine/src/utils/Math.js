Math.gNormalize = (min = 0, max = 100, value = 50) => {
    return (value - min) / (max - min)
}

Math.gClamp = (min = 0, max = 1, value = 0.5) => {
    return value <= min ? min : value >= max ? max : value
}
Math.gLerp = (start, end, time) => {
    return (1 - time) * start + time * end
}


Math.gClampReset = (min = 0, max = 1, resetVal = 0.5, value = 0.5) => {
    return value <= min ? resetVal : value >= max ? resetVal : value
}

