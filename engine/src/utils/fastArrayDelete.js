/**
 *
 * @param {array} array that you want to delete an item from
 * @param {integer} index of the item you want to delete
 */

export default function fastArrayDelete(arr, fromIndex) {
  if (arr.length >= 30000) {
    let element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(arr[arr.length - 1], 0, element);
    arr.pop();
  } else {
    arr.splice(fromIndex, 1);
  }
}
