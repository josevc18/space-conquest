export default class Vec2 {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    fromArray(arr = [0, 0]) {
        this.x = arr[0];
        this.y = arr[1];
        return { x: this.x, y: this.y }
    }
    fromObject(obj = { x: 0, y: 0 }) {
        this.x = obj[x];
        this.y = obj[y];
        return { x: this.x, y: this.y }
    }
    toArray() {
        return [this.x, this.y]
    }
    toObject() {
        return { x: this.x, y: this.y }
    }

    // manipulation
    addX(vec) {
        this.x += vec.x
        return this;
    }
    addY(vec) {
        this.y += vec.y
        return this;
    }
    add(vec) {
        this.y += vec.y
        this.x += vec.x
        return this;
    }
    // manipulation
    subtractX(vec) {
        this.x -= vec.x
        return this;
    }
    subtractY(vec) {
        this.y -= vec.y
        return this;
    }
    subtract(vec) {
        this.y -= vec.y
        this.x -= vec.x
        return this;
    }

    // manipulation
    multiplyX(vec) {
        this.x *= vec.x
        return this;
    }
    multiplyY(vec) {
        this.y *= vec.y
        return this;
    }
    multiply(vec) {
        this.y *= vec.y
        this.x *= vec.x
        return this;
    }


    // manipulation
    divideX(vec) {
        this.x /= vec.x
        return this;
    }
    divideY(vec) {
        this.y /= vec.y
        return this;
    }
    divide(vec) {
        this.y /= vec.y
        this.x /= vec.x
        return this;
    }

    invertX() {
        this.x = -this.x
        return this
    }
    invertY() {
        this.y = -this.y
        return this
    }
    invert() {
        this.y = -this.y
        this.x = -this.x
        return this
    }
    roundX() {
        this.x = Math.round(this.x)
        return this
    }
    roundY() {
        this.y = Math.round(this.y)
        return this
    }

    round() {
        this.y = Math.round(this.y)
        this.x = Math.round(this.x)
        return this
    }

    ceilX() {
        this.x = Math.ceil(this.x)
        return this
    }
    ceilY() {
        this.y = Math.ceil(this.y)
        return this
    }

    ceil() {
        this.y = Math.ceil(this.y)
        this.x = Math.ceil(this.x)
        return this
    }

    floorX() {
        this.x = Math.floor(this.x)
        return this
    }
    floorY() {
        this.y = Math.floor(this.y)
        return this
    }

    floor() {
        this.y = Math.floor(this.y)
        this.x = Math.floor(this.x)
        return this
    }

    randomizeX(topLeft, bottomRight) {
        this.x = Math.random() * (bottomRight - topLeft) + topLeft
        return this
    }

    randomizeY(topLeft, bottomRight) {
        this.y = Math.random() * (bottomRight - topLeft) + topLeft
        return this
    }

    randomize(topLeft, bottomRight) {
        this.x = Math.random() * (bottomRight - topLeft) + topLeft
        this.y = Math.random() * (bottomRight - topLeft) + topLeft
        return this
    }
    clampX(min, max) {
        this.x > max ? this.x = max : this.x;
        this.x < min ? this.x = min : this.x;
        return this
    }

    clampY(min, max) {
        this.y > max ? this.y = max : this.y;
        this.y < min ? this.y = min : this.y;
        return this
    }

    clamp(minX, maxX, minY, maxY) {
        this.x > maxX ? this.x = maxX : this.x;
        this.x < minX ? this.x = minX : this.x;
        this.y > maxY ? this.y = maxY : this.y;
        this.y < minY ? this.y = minY : this.y;
        return this
    }

    // porduct
    cross(vec) {
        return this.x * vec.x + this.y * vec.y
    }

    dot(vec) {
        return (this.x * vec.y) - (this.y * vec.x)
    }
    normalize() {
        let length = this.magnitude();
        if (length === 0) {
            this.x = 1;
            this.y = 0;
        } else {
            this.divide({ x: length, y: length });
        }
        return this;
    };
    lengthSqrt() {
        return Math.pow(this.x, 2) + Math.pow(this.x, 2)
    }

    magnitude() {
        Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.x, 2))
    }
    distanceX(vec) {
        return this.x - vec.x
    }

    distanceY(vec) {
        return this.y - vec.y
    }
    distance(vec) {
        return Math.hypot(this.x - vec.x, this.y - vec.y)
    }
    absDistanceX(vec) {
        return Math.abs(this.x - vec.x)
    }

    absDistanceY(vec) {
        return Math.abs(this.y - vec.y)
    }

    absDistance(vec) {
        return Math.abs(Math.hypot(this.x - vec.x, this.y - vec.y))
    }
    hAngleRads() {
        return Math.atan2(this.y, this.x)
    }
    vAngleRads() {
        return Math.atan2(this.x, this.y)
    }
    hAngleDegs() {
        return Math.atan2(this.y, this.x) * 180 / Math.PI
    }
    vAngleDegs() {
        return Math.atan2(this.x, this.y) * 180 / Math.PI
    }

    lerpX(vec, ratio = 0.5) {
        return this.x * (1 - ratio) + vec.x * ratio;
    }

    lerpY(vec, ratio = 0.5) {
        return this.y * (1 - ratio) + vec.y * ratio;
    }

    lerpY(vec, ratio = 0.5) {
        return this.y * (1 - ratio) + vec.y * ratio;
    }

    lerp(vec, ratio = 0.5) {
        this.x * (1 - ratio) + vec.x * ratio;
        this.y * (1 - ratio) + vec.y * ratio;
        return this
    }

    pointToRads(vec) {
        let dx = this.x - vec.x
        let dy = this.y - vec.y
        return Math.atan2(dy, dx)
    }

    pointToDegs(vec) {
        let dx = this.x - vec.x
        let dy = this.y - vec.y
        return Math.atan2(dy, dx) * 180 / Math.PI
    }

    movePointingToDegs(angleDegs = 0, dx = 0, dy = 0) {
        this.x += dx * Math.sin(angleDegs)
        this.y -= dy * Math.cos(angleDegs)
        return this
    }

}