export default class SpritesSheet {
    constructor({ path = '' }) {
        this.path = path
        this.spriteElem = document.createElement('img');
        this.spriteElem.src = this.path
    }
}