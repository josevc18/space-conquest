import Vec2 from "../utils/vec2";
import '../utils/Math'
export default class Layer {
    constructor({
        container = document.body,
        id = 'canvas',
        width = 250,
        height = 250,
        layered = true,
        zIndex = 1,
        context = "2d",
        world = { width: 600, height: 600 },
    } = {}) {
        this.id = id
        this.container = container;
        this.canvas = document.createElement('canvas');
        this.canvas.height = height
        this.canvas.width = width
        this.canvas.id = this.id
        this.container.appendChild(this.canvas)
        this.ctx = this.canvas.getContext(context);
        this.layered = layered;
        this.view = new Vec2(0, 0)
        this.world = world

        if (this.layered) {
            this.canvas.style.position = 'absolute'
            this.canvas.style.left = 0
            this.canvas.style.top = 0
            this.canvas.style.zIndex = zIndex
        }
    }
    toWindowSize() {
        this.canvas.height = window.innerHeight;
        this.canvas.width = window.innerWidth;
        window.addEventListener('resize', () => {
            this.canvas.height = window.innerHeight;
            this.canvas.width = window.innerWidth;
        })
        return this
    }
    move(x, y) {
        this.ctx.translate(x, y)
        this.view.x -= x
        this.view.y -= y
        return this
    }
    moveInWorldSpace(x, y) {

        this.view.x -= x
        this.view.y -= y
        this.view.x = Math.gClamp(-this.world.width, this.world.height, this.view.x)
        this.view.y = Math.gClamp(-this.world.width, this.world.height, this.view.y)
        if (this.view.x > -this.world.width && this.view.x < this.world.width) {

            this.ctx.translate(x, 0)
        }
        if (this.view.y > -this.world.height && this.view.y < this.world.height) {

            this.ctx.translate(0, y)
        }

        return this
    }
    rotate(x, y, radians) {
        this.ctx.translate(x, y);
        this.ctx.rotate(radians);
        this.ctx.translate(-x, -y);
    }
    toWorldSpace(vec) {
        return new Vec2(vec.x - this.view.x, vec.y - this.view.y)
    }
    clear({ topLeft = 0, bottomRight = 0, width = window.innerWidth, height = window.innerHeight } = {}) {
        this.ctx.clearRect(topLeft, bottomRight, width, height)
        return this
    }
    clearWorldSpace(x = -this.world.width / 2, y = -this.world.height / 2) {
        this.ctx.clearRect(x, y, this.world.width, this.world.height)
        return this
    }


}