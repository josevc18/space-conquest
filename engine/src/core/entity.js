import fastArrayDelete from '../utils/fastArrayDelete'

/**
 *
 *
 * @export
 * @class Entity
 */
export default class Entity {

    /**
     * Creates an instance of Entity.
     * @param {*} [{
     *         tags = new Set().add('Entity'),
     *         entityManager = undefined,
     *         index = 0 }={}]
     * @memberof Entity
     */
    constructor({
        tags = new Set().add('Entity'),
        entityManager = undefined,
        index = 0 } = {}) {
        this.index = index
        this.tags = tags;
        this.entityManager = entityManager;
        this.rendererTime = {}
    }

    /**
     *
     *
     * @param {string} [tag='']
     * @return {*} 
     * @memberof Entity
     */
    addTag(tag = '') {
        this.tags.add(tag)
        return this
    }

    /**
     *
     *
     * @param {string} [tag='']
     * @return {*} 
     * @memberof Entity
     */
    removeTag(tag = '') {
        this.tags.delete(tag)
        return this
    }

    /**
     *
     *@description "seeks caller entity inside the manager entity list and removes the entity from it"
     * @memberof Entity
     */
    destroy() {

        fastArrayDelete(this.entityManager.entities,
            this.entityManager.entities.indexOf(this))
    }

    /**
     *
     *
     * @return {*} 
     * @memberof Entity
     */


    init() {
        return true
    }

    /**
     *
     *
     * @param {*} time
     * @return {*} 
     * @memberof Entity
     */
    update(time) {
        return true
    }
}