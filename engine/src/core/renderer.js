
/**
 *
 *
 * @export
 * @class Renderer
 */
export default class Renderer {

    /**
     * Creates an instance of Renderer.
     * @param {number} [targetFramerate=60]
     * @memberof Renderer
     */
    constructor(targetFramerate = 60) {
        this.targetFramerate = targetFramerate;
        this.interval = 1000 / this.targetFramerate;
        this.then = performance.now();
        this.now = 0;
        this.deltaTime = 0;
    }

    /**
     *
     *
     * @return {*} 
     * @memberof Renderer
     */
    start() {
        return false
    }

    init() {
        this.start()
        const render = (timeStamp) => {
            requestAnimationFrame(render);
            this.now = performance.now();
            this.deltaTime = this.now - this.then;
            if (this.deltaTime >= this.interval) {
                this.update({
                    deltaTime: this.deltaTime,
                    then: this.then,
                    now: this.now,
                    interval: this.interval,
                    timeStamp: timeStamp,
                    targetFramerate: this.targetFramerate,
                });
            }
            this.then = this.now - (this.deltaTime % this.interval);
        };
        render();
        return this
    }
    update(time) {
        return false;
    }
}
