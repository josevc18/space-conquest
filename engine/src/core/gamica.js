import Renderer from './renderer'
import EntityManager from './entityManager'
import Entity from './entity'
import Layer from './layer'
import SpriteSheet from './sprite'

export const gamica = { SpriteSheet, Renderer, EntityManager, Entity, Layer }