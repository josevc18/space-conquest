

/**
 *
 *
 * @export
 * @class EntityManager
 */
export default class EntityManager {
    constructor() {
        this.entities = [];
    }

    /**
     *
     *
     * @param {*} entity
     * @return {*} 
     * @memberof EntityManager
     */
    addEntity(entity) {
        entity.entityManager = this;
        entity.index = this.entities.length
        entity.start()
        this.entities.push(entity)
        return this
    }


    /**
     *
     *
     * @param {*} [filterFunction=new Function()]
     * @return {*} 
     * @memberof EntityManager
     */
    queryEntities(filterFunction = new Function()) {
        let acuEntities = []
        for (let i = 0; i < this.entities.length; i++) {
            if (filterFunction(this.entities[i], i)) {
                acuEntities.push(this.entities[i])
            }
        }
        return acuEntities
    }

    /**
     *
     *
     * @memberof EntityManager
     */
    start() {
        for (let i = 0; i < this.entities.length; i++) {
            this.entities[i].start()
        }
    }

    /**
     *
     *
     * @param {*} time
     * @memberof EntityManager
     */
    update(time) {
        for (let i = 0; i < this.entities.length; i++) {

            this.entities[i].rendererTime = time
            this.entities[i].update(time)
        }
    }

}